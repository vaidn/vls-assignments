** License choice explination

I chose the MIT license agreement because it is a stright forward agreement that gives permission to all users. The agreements
conditions are only to preserve copyright and license notices which is fine for a project such as this.


** Running instructions

To run the project, open the file in Visual Studio code. Once the project is opened, navigate to the top right of the screen and press the "Run" button. This will run the
project on the users localhost.